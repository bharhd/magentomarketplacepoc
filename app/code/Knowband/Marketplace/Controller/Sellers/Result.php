<?php
 
namespace Knowband\Marketplace\Controller\Sellers;
 
use Magento\Framework\App\Action\Context;
use Psr\Log\LoggerInterface;
use Magento\Customer\Model\Session;

class Result extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
 
    /**
     * @var $_logger
     */
    protected $_logger;

    /**
     * @var Session
     */
    protected $session; 
    
    
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        LoggerInterface $logger,
        Session $customerSession)
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_logger = $logger;
        $this->session = $customerSession;                       
        parent::__construct($context);
    }
 
    public function execute()
    {        
     // echo "<pre>";  print_r($this->getRequest()->getPostValue()); exit;

        try { 
            $postParams = array();
            if ($this->getRequest()->isPost()) {
                $postParams = $this->getRequest()->getPostValue();
            }

            $this->session->setSearchPostParams($postParams);
        } catch (\Exception $e) {
            $this->_logger->critical($e->getMessage());
            $this->messageManager->addError(__('Unable to search.'));  
            return $resultRedirect->setPath('/');
        }    

        $resultPage = $this->_resultPageFactory->create();
        return $resultPage;
    }
}
