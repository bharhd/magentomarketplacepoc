<?php

namespace Knowband\Marketplace\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface {

  public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        
    if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $installer = $setup;
            $installer->startSetup();
                $setup->getConnection()
                    ->addColumn(
                    $setup->getTable('vss_mp_seller_entity'),
                    'seller_code',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                         'length' => 255,
                         'nullable' => true,
                        'comment' =>'Seller Code'
                    ]
                );
            $installer->endSetup();
      }
        
}
