<?php

namespace Knowband\Marketplace\Block;

use Magento\Customer\Model\Session;

class Search extends \Magento\Framework\View\Element\Template {


    protected $session;


    public function __construct(
            \Magento\Framework\View\Element\Template\Context $context,
            \Magento\Framework\ObjectManagerInterface $objectManager,
            \Knowband\Marketplace\Model\Product $mpProductToSellerModel,
            \Knowband\Marketplace\Helper\Setting $mpSettingHelper,
            \Knowband\Marketplace\Helper\Data $mpDataHelper,
            \Knowband\Marketplace\Helper\Reports $mpReportsHelper,
            \Knowband\Marketplace\Helper\Log $mpLogHelper,
            \Magento\Catalog\Model\ProductFactory $_productloader,
            Session $customerSession
    ) {
        $this->_objectManager = $objectManager;
        $this->mp_productToSellerModel = $mpProductToSellerModel;
        $this->mp_settingHelper = $mpSettingHelper;
        $this->mp_dataHelper = $mpDataHelper;
        $this->mp_reportsHelper = $mpReportsHelper;
        $this->_productloader = $_productloader;
        $this->mp_logHelper = $mpLogHelper;
        $this->session = $customerSession;
        parent::__construct($context);
    }
    
    
     public function getAllSellerEnabledProducts() {
        $sellerProducts = [];
        $website_id = 1;

       // echo "hi"; exit();
        try {

            $productCollection = $this->mp_productToSellerModel->getCollection()
                    ->addFieldToFilter('website_id', (int) $website_id)
                    ->addFieldToFilter('approved', 1)
                    ->addFieldToSelect('product_id')->distinct(true);


            $products = $productCollection->getData();

          //  echo "<pre>Hi"; print_r($productCollection->getData()); exit;

            unset($productCollection);

            foreach ($products as $pro) {
                $sellerProducts[] = $pro['product_id'];
            }

            if (empty($sellerProducts)) {
                return [0];
            }

        } catch (\Exception $ex) {
            $this->mp_logHelper->createFileAndWriteLogData(
                    \Knowband\Marketplace\Helper\Log::INFOTYPEERROR, 'Helper Product::getSellerEnabledProducts()', $ex->getMessage()
            );
        }

        return $sellerProducts;
    }


    public function getProductName($produtId)
    {
        return $this->_productloader->create()->load($produtId);
    }

    public function getPostActionUrl()
    {
        return $this->getUrl('marketplace/sellers/result');
    }



}
