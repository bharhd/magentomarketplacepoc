<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Knowband\Marketplace\Block\Form;


/**
 * Customer register form block
 *
 * @api
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @since 100.0.2
 */
class Register extends \Magento\Customer\Block\Form\Register
{
   
    protected function _prepareLayout()
    {   
        
         if($this->getRequest()->getParam('did') == 1){
            $this->pageConfig->getTitle()->set(__('Create New Dealer Account'));
        }else{
        $this->pageConfig->getTitle()->set(__('Create New Customer Account')); }
    }

   
}
